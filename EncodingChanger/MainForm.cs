﻿using System;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace EncodingChanger
{
    public partial class MainForm : Form
    {
        private readonly string _openFileTextBoxDefault;
        private readonly string _saveFileTextBoxDefault;

        public MainForm()
        {
            InitializeComponent();
            _openFileTextBoxDefault = openFileTextBox.Text;
            _saveFileTextBoxDefault = saveFileTextBox.Text;
        }

        private void OpenFileBrowseButton_Click(object sender, EventArgs e)
        {
            var res = openFileDialog.ShowDialog();
            switch (res)
            {
                case (DialogResult.OK):

                    openFileTextBox.Text = openFileDialog.FileName;
                    break;
                case (DialogResult.Abort):
                case (DialogResult.Cancel):
                case (DialogResult.None):
                    break;
                default:
                    MessageBox.Show("Invalid Dialog Result", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
            }
        }

        private void SaveFileBrowseButton_Click(object sender, EventArgs e)
        {
            var res = saveFileDialog.ShowDialog();
            switch (res)
            {
                case (DialogResult.OK):
                    saveFileTextBox.Text = saveFileDialog.FileName;
                    break;
                case (DialogResult.Abort):
                case (DialogResult.Cancel):
                case (DialogResult.None):
                    break;
                default:
                    MessageBox.Show("Invalid Dialog Result", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
            }
        }

        private void ProcessButton_Click(object sender, EventArgs e)
        {
            try
            {
                Process();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                MessageBox.Show("File has been written", "OK", MessageBoxButtons.OK, MessageBoxIcon.None);
            }
        }

        

        private void SaveFileTextBox_Enter(object sender, EventArgs e)
        {
            if (saveFileTextBox.Text.Equals(_saveFileTextBoxDefault))
                saveFileTextBox.Text = "";
        }

        private void SaveFileTextBox_Leave(object sender, EventArgs e)
        {
            if (saveFileTextBox.Text.Trim().Length == 0)
                saveFileTextBox.Text = _saveFileTextBoxDefault;
        }

        private void OpenFileTextBox_Enter(object sender, EventArgs e)
        {
            if (openFileTextBox.Text.Equals(_openFileTextBoxDefault))
                openFileTextBox.Text = "";
        }

        private void OpenFileTextBox_Leave(object sender, EventArgs e)
        {
            if (openFileTextBox.Text.Trim().Length == 0)
                openFileTextBox.Text = _openFileTextBoxDefault;
        }


        #region Helpers

        private static void SaveToAnotherEncoding(string sourcePath, string destPath, Encoding sourceEncoding, Encoding
            neededEncoding)
        {
            try
            {
                var contents = File.ReadAllLines(sourcePath, sourceEncoding);
                if (sourcePath.Equals(destPath))
                {
                    File.Move(sourcePath, sourcePath + ".bkp");
                }

                File.WriteAllLines(destPath, contents, neededEncoding);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Process()
        {
            if (openFileTextBox.Text.Equals(_saveFileTextBoxDefault) || openFileTextBox.Text.Trim().Length == 0 ||
                saveFileTextBox.Text.Equals(_saveFileTextBoxDefault) || saveFileTextBox.Text.Trim().Length == 0)
            {
                throw new Exception("Input source and Destination");
            }
            else if (sourceEncodingComboBox.Text.Equals(destEncodingComboBox.Text))
            {
                throw new Exception("Encodings are already same");
            }
            else
            {
                var sourceEncoding = Encoding.GetEncoding(sourceEncodingComboBox.Text);
                var destEncoding = Encoding.GetEncoding(destEncodingComboBox.Text);
                SaveToAnotherEncoding(openFileTextBox.Text, saveFileTextBox.Text, sourceEncoding, destEncoding);
            }
        }


        #endregion

        private void HelpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //TODO--вывод инструкции
        }

        private void AboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //TODO--вывод информации о приложении
        }
    }
}