﻿using System.Windows.Forms;

namespace EncodingChanger
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.openFileTextBox = new System.Windows.Forms.TextBox();
            this.openFileBrowseButton = new System.Windows.Forms.Button();
            this.saveFileBrowseButton = new System.Windows.Forms.Button();
            this.saveFileTextBox = new System.Windows.Forms.TextBox();
            this.sourceEncodingComboBox = new System.Windows.Forms.ComboBox();
            this.destEncodingComboBox = new System.Windows.Forms.ComboBox();
            this.processButton = new System.Windows.Forms.Button();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(379, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.HelpToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItem_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            // 
            // openFileTextBox
            // 
            this.openFileTextBox.Location = new System.Drawing.Point(12, 39);
            this.openFileTextBox.Name = "openFileTextBox";
            this.openFileTextBox.Size = new System.Drawing.Size(274, 20);
            this.openFileTextBox.TabIndex = 1;
            this.openFileTextBox.Text = "Enter source file path.....";
            this.openFileTextBox.Enter += new System.EventHandler(this.OpenFileTextBox_Enter);
            this.openFileTextBox.Leave += new System.EventHandler(this.OpenFileTextBox_Leave);
            // 
            // openFileBrowseButton
            // 
            this.openFileBrowseButton.Location = new System.Drawing.Point(292, 39);
            this.openFileBrowseButton.Name = "openFileBrowseButton";
            this.openFileBrowseButton.Size = new System.Drawing.Size(57, 20);
            this.openFileBrowseButton.TabIndex = 2;
            this.openFileBrowseButton.Text = "Browse...";
            this.openFileBrowseButton.UseVisualStyleBackColor = true;
            this.openFileBrowseButton.Click += new System.EventHandler(this.OpenFileBrowseButton_Click);
            // 
            // saveFileBrowseButton
            // 
            this.saveFileBrowseButton.Location = new System.Drawing.Point(292, 121);
            this.saveFileBrowseButton.Name = "saveFileBrowseButton";
            this.saveFileBrowseButton.Size = new System.Drawing.Size(57, 20);
            this.saveFileBrowseButton.TabIndex = 4;
            this.saveFileBrowseButton.Text = "Browse...";
            this.saveFileBrowseButton.UseVisualStyleBackColor = true;
            this.saveFileBrowseButton.Click += new System.EventHandler(this.SaveFileBrowseButton_Click);
            // 
            // saveFileTextBox
            // 
            this.saveFileTextBox.AutoCompleteCustomSource.AddRange(new string[] {
            "koi8-r",
            "windows-1251",
            "utf-8",
            "cp866"});
            this.saveFileTextBox.Location = new System.Drawing.Point(12, 121);
            this.saveFileTextBox.Name = "saveFileTextBox";
            this.saveFileTextBox.Size = new System.Drawing.Size(274, 20);
            this.saveFileTextBox.TabIndex = 3;
            this.saveFileTextBox.Text = "Enter destination path.....";
            this.saveFileTextBox.Enter += new System.EventHandler(this.SaveFileTextBox_Enter);
            this.saveFileTextBox.Leave += new System.EventHandler(this.SaveFileTextBox_Leave);
            // 
            // sourceEncodingComboBox
            // 
            this.sourceEncodingComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "koi8-r",
            "windows-1251",
            "utf-8",
            "ibm-866"});
            this.sourceEncodingComboBox.FormattingEnabled = true;
            this.sourceEncodingComboBox.Items.AddRange(new object[] {
            "koi8-r",
            "windows-1251",
            "utf-8",
            "cp866"});
            this.sourceEncodingComboBox.Location = new System.Drawing.Point(13, 66);
            this.sourceEncodingComboBox.Name = "sourceEncodingComboBox";
            this.sourceEncodingComboBox.Size = new System.Drawing.Size(130, 21);
            this.sourceEncodingComboBox.TabIndex = 5;
            this.sourceEncodingComboBox.Text = "windows-1251";
            // 
            // destEncodingComboBox
            // 
            this.destEncodingComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "koi8-r",
            "windows-1251",
            "utf-8",
            "ibm-866"});
            this.destEncodingComboBox.FormattingEnabled = true;
            this.destEncodingComboBox.Items.AddRange(new object[] {
            "koi8-r",
            "windows-1251",
            "utf-8",
            "ibm-866"});
            this.destEncodingComboBox.Location = new System.Drawing.Point(12, 147);
            this.destEncodingComboBox.Name = "destEncodingComboBox";
            this.destEncodingComboBox.Size = new System.Drawing.Size(130, 21);
            this.destEncodingComboBox.TabIndex = 6;
            this.destEncodingComboBox.Text = "koi8-r";
            // 
            // processButton
            // 
            this.processButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.processButton.Location = new System.Drawing.Point(292, 181);
            this.processButton.Name = "processButton";
            this.processButton.Size = new System.Drawing.Size(57, 29);
            this.processButton.TabIndex = 7;
            this.processButton.Text = "Go";
            this.processButton.UseVisualStyleBackColor = true;
            this.processButton.Click += new System.EventHandler(this.ProcessButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(379, 223);
            this.Controls.Add(this.processButton);
            this.Controls.Add(this.destEncodingComboBox);
            this.Controls.Add(this.sourceEncodingComboBox);
            this.Controls.Add(this.saveFileBrowseButton);
            this.Controls.Add(this.saveFileTextBox);
            this.Controls.Add(this.openFileBrowseButton);
            this.Controls.Add(this.openFileTextBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Encoding Changer";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.TextBox openFileTextBox;
        private System.Windows.Forms.Button openFileBrowseButton;
        private System.Windows.Forms.Button saveFileBrowseButton;
        private System.Windows.Forms.TextBox saveFileTextBox;
        private System.Windows.Forms.ComboBox sourceEncodingComboBox;
        private System.Windows.Forms.ComboBox destEncodingComboBox;
        private System.Windows.Forms.Button processButton;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
    }
}

